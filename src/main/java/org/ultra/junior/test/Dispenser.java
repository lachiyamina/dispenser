package org.ultra.junior.test;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class Dispenser {
    public String DispenserName;
    public int DispenserStock;

    public ArrayList<Double> CoinsAccepted = new ArrayList();
    public ArrayList<Drink> Drinks = new ArrayList();
    public ArrayList<Integer> DrinksCodes = new ArrayList();


    public Dispenser(String DispenserName) {
        this.CoinsAccepted.add(0.05);
        this.CoinsAccepted.add(0.10);
        this.CoinsAccepted.add(0.20);
        this.CoinsAccepted.add(0.50);
        this.CoinsAccepted.add(1.0);
        this.CoinsAccepted.add(2.0);
        this.DispenserName = DispenserName;
    }

    public ArrayList<Double> getCoinsAccepted() {
        return this.CoinsAccepted;
    }

    public void setCoinsAccepted(ArrayList<Double> coinsAccepted) {
        this.CoinsAccepted = coinsAccepted;
    }

    public ArrayList<Drink> getDrinks() {
        return this.Drinks;
    }

    public void setDrinks(ArrayList<Drink> Drinks) {
        this.Drinks = Drinks;
    }

    public void addDrinks(Drink drink){
        this.Drinks.add(drink);
    }

    public String DisplayDispenserStock() {
        String DisplaytStock = "The stock of the dispenser is : \n";
        for (int i = 0; i < this.Drinks.size(); i++) {
            DispenserStock = this.Drinks.get(i).getDrinkStock();
            DisplaytStock += (this.Drinks.get(i).getDrinkName() + " : " + DispenserStock + "\n").toString();
        }
        return DisplaytStock;
    }

    public ArrayList<Integer> getDrinksCodes() {
        for (int i = 0; i < this.Drinks.size(); i++) {
            DrinksCodes.add(this.Drinks.get(i).getDrinkCode());
        }
        return DrinksCodes;
    }

    public Drink getSpecificDrinkByCode(int CodeDrink) {
        for (Drink drink : this.getDrinks()) {
            if (drink.getDrinkCode() == CodeDrink) {
                return drink;
            }
        }
        return null;
    }
    public void DecreaseDrinkStock(int randomCode) {
        for (Drink drink : this.getDrinks()) {
            if (drink.getDrinkCode() == randomCode) {
                drink.setDrinkStock(drink.getDrinkStock() - 1);
            }
        }
    }

    public void IncreaseDrinkStock(int DrinkCode, int stock) {
        for (Drink drink : this.getDrinks()) {
            if (drink.getDrinkCode() == DrinkCode) {
                drink.setDrinkStock(drink.getDrinkStock() + stock);
            }
        }
    }

    public void  MoneyChange (Double coins, ArrayList<Double> EmployeeCoins) {
        while (coins >= 1.0) {
           coins =  addCoins(coins, EmployeeCoins, 1.0);
        }
        while (coins >= 0.50) {
            coins = addCoins(coins, EmployeeCoins, 0.50);
        }
        while (coins < 0.50 && coins >= 0.10) {
            coins =  addCoins(coins, EmployeeCoins, 0.10);
        }
        while (coins < 0.1 && coins > 0.0) {
            coins = addCoins(coins, EmployeeCoins, 0.05);
        }
    }

    public Double addCoins(Double coins, ArrayList<Double> EmployeeCoins, Double coinsToreturn){
        EmployeeCoins.add(coinsToreturn);
        coins = Double.parseDouble((new DecimalFormat("0.00").format(coins - coinsToreturn)).replace(",", "."));
        return coins;
    }


}
