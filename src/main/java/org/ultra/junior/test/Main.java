package org.ultra.junior.test;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        System.out.println("Hello world!");
        Drink Coca = new Drink(1, "Coca", 001, 1);
        Drink Redbull = new Drink(1.25, "Redbull", 002, 1);
        Drink Water = new Drink(0.50, "Water", 003, 1);
        Drink Orange = new Drink(1.95, "Orange juice", 004, 1);

        Dispenser dispenserSdworx = new Dispenser("SDWorx dispenser");
        dispenserSdworx.addDrinks(Coca);
        dispenserSdworx.addDrinks(Redbull);
        dispenserSdworx.addDrinks(Water);
        dispenserSdworx.addDrinks(Orange);

        //Dispenser stock after the order
        System.out.println(dispenserSdworx.DisplayDispenserStock());

        System.out.println("**** Good Morning :), Order your drink ***** \n");
        Employee Yamina = new Employee();
        Yamina.DrinkOrder(dispenserSdworx);
        //Dispenser stock after the order
        System.out.println(dispenserSdworx.DisplayDispenserStock());

        //Add a stock to the dispenser for an existing drink
        dispenserSdworx.IncreaseDrinkStock(001, 2);

        //Dispenser stock after increasing the stock :
        System.out.println("Dispenser stock after increasing the stock of a dispenser");
        System.out.println(dispenserSdworx.DisplayDispenserStock());

        // Oder a second time
        Yamina.DrinkOrder(dispenserSdworx);

    }
}