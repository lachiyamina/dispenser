package org.ultra.junior.test;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Random;

public class Employee {

    public ArrayList<Double> EmployeeCoins = new ArrayList();
    public ArrayList<Integer> DrinksCodes = new ArrayList();

    public ArrayList<Integer> CodeListInDispenser;
    public ArrayList<Double> CoinsListInDispenser;
    public Random rand;
    public int randomDrinkCode;
    public Double randomCoins;
    public Double price;
    public Double CoinsInserted;

    public Employee() {

        this.DrinksCodes.add(001);
        this.DrinksCodes.add(002);
        this.DrinksCodes.add(003);
        this.DrinksCodes.add(004);
        this.DrinksCodes.add(005);

        this.EmployeeCoins.add(1.0);
        this.EmployeeCoins.add(2.0);
        this.EmployeeCoins.add(0.50);
        this.EmployeeCoins.add(0.10);
        this.EmployeeCoins.add(0.05);

    }

    public ArrayList<Double> getEmployeeCoins() {
        return EmployeeCoins;
    }

    public ArrayList<Integer> getDrinksCodes() {
        return DrinksCodes;
    }

    public void DrinkOrder(Dispenser dispenser) {
        // Insert the coins
        randomCoins = getEmployeeCoins(this.EmployeeCoins, dispenser);
        CoinsInserted = randomCoins;
        this.EmployeeCoins.remove(this.EmployeeCoins.indexOf(randomCoins));
        // Select a code
        randomDrinkCode = getDrinkCodeEmployee(dispenser);
        if (dispenser.getSpecificDrinkByCode(randomDrinkCode).getDrinkStock() > 0) {
            // get the price of the code drink
            Double DrinkPrice = dispenser.getSpecificDrinkByCode(randomDrinkCode).getDrinkPrice();
            System.out.println("The price of the drink is : " + DrinkPrice + " $ \n");

            while (DrinkPrice > CoinsInserted) {
                System.out.println("You have " + (DrinkPrice - CoinsInserted) + " $ left to pay \n");
                randomCoins= getEmployeeCoins(this.EmployeeCoins, dispenser);
                this.EmployeeCoins.remove(this.EmployeeCoins.indexOf(randomCoins));
                CoinsInserted = CoinsInserted + randomCoins ;

            }
            // Cancel the order or not
            System.out.println("Do you want to cancel the order");
            Boolean cancelOrder = cancelOrder();
            if (cancelOrder) {
                System.out.println(cancelOrder);
                dispenser.MoneyChange(CoinsInserted, this.EmployeeCoins);
                System.out.println("Your coins : " + this.EmployeeCoins);
            } else {
                System.out.println(cancelOrder);
                if (DrinkPrice < CoinsInserted) {
                    Double CoinsToReturn = Double.parseDouble((new DecimalFormat("0.00").format(CoinsInserted - DrinkPrice)).replace(",", "."));
                    dispenser.MoneyChange(CoinsToReturn, this.EmployeeCoins);
                    System.out.println("Take your monney " + CoinsToReturn + "$ and your " + dispenser.getSpecificDrinkByCode(randomDrinkCode).getDrinkName() + " drink \n");
                    System.out.println("Your coins : " + this.EmployeeCoins);
                    dispenser.DecreaseDrinkStock(randomDrinkCode);

                } else {
                    if (DrinkPrice.compareTo(CoinsInserted) == 0) {
                        System.out.println("Take your " + dispenser.getSpecificDrinkByCode(randomDrinkCode).getDrinkName() + " drink \n");
                        dispenser.DecreaseDrinkStock(randomDrinkCode);
                    }

                }
            }
            System.out.println("Thank you for your order, Good Bye ! \n");
        } else {
            System.out.println("There is no stock for this drink, thank you");
            this.EmployeeCoins.add(randomCoins);
            System.out.println("Your coins : " + this.EmployeeCoins);
        }

    }

    public Double getEmployeeCoins(ArrayList<Double> Coins, Dispenser dispenser) {
        System.out.println("****** Please insert the coins ****** \n 	  ........ \n");
        rand = new Random();
        randomCoins = Coins.get(rand.nextInt(Coins.size()));
        System.out.println("You have inserted : " + randomCoins + " $\n");
        this.CoinsListInDispenser = dispenser.getCoinsAccepted();
        while (!this.CoinsListInDispenser.contains(randomCoins)) {
            System.out.println("Please enter either : 1$ , 2$ , 0.20$ ! \n");
            rand = new Random();
            randomCoins = Coins.get(rand.nextInt(Coins.size()));
        }
        return randomCoins;
    }

    public int getDrinkCodeEmployee(Dispenser dispenser) {
        System.out.println("****** Please enter the code of the drink !  ********\n");
        rand = new Random();
        randomDrinkCode = this.DrinksCodes.get(rand.nextInt(this.DrinksCodes.size()));
        this.CodeListInDispenser = dispenser.getDrinksCodes();
        while (!this.CodeListInDispenser.contains(randomDrinkCode)) {
            System.out.println("******  Error ! the code is not find ! Please enter a right code !	******\n");
            rand = new Random();
            randomDrinkCode = this.DrinksCodes.get(rand.nextInt(this.DrinksCodes.size()));
        }
        System.out.println("The code of the drink that you have selected is : " + randomDrinkCode + ":" + dispenser.getSpecificDrinkByCode(randomDrinkCode).getDrinkName() + "\n");
        return randomDrinkCode;
    }

    public Boolean cancelOrder() {
        rand = new Random();
        return rand.nextBoolean();
    }
}
