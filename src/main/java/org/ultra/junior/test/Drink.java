package org.ultra.junior.test;

public class Drink {
    public double DrinkPrice;
    public String DrinkName;
    public int DrinkCode;
    public int DrinkStock;

    public double getPrice() {
        return DrinkPrice;
    }

    public double getDrinkPrice() {
        return DrinkPrice;
    }

    public void setDrinkPrice(double drinkPrice) {
        DrinkPrice = drinkPrice;
    }

    public String getDrinkName() {
        return DrinkName;
    }

    public void setDrinkName(String drinkName) {
        DrinkName = drinkName;
    }

    public int getDrinkCode() {
        return DrinkCode;
    }

    public void setDrinkCode(int drinkCode) {
        DrinkCode = drinkCode;
    }

    public int getDrinkStock() {
        return DrinkStock;
    }

    public void setDrinkStock(int drinkStock) {
        DrinkStock = drinkStock;
    }

    public Drink(double price, String name, int code, int stock) {
        this.DrinkPrice = price;
        this.DrinkName = name;
        this.DrinkCode = code;
        this.DrinkStock = stock;
    }

}
